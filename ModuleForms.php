<?php

if (!defined('_PS_VERSION_'))
    exit;

class ModuleForms
{
	private $module;
	private $translations = array();

	public function __construct(CustomContact $module)
	{
		$this->module = $module;
		$this->translations = $this->module->returnTranslations();
	}

	public function getAdminForm()
   	{
   		$fields_form[0]['form'] = array(
   			   'tinymce' => true,
               'legend' => array(
                   'title' => $this->translations['admin_form_title'],
               ),
               'input' => array(                  
                   array(
                       'type' => 'text',
                       'label'     => $this->translations['admin_top_title'],
                       'name' => 'custom_contact_title',
                       'required'  => false,            
                   ),
                   array(
                       'type' => 'textarea',
                       'label'     => $this->translations['admin_content'],
                       'name' => 'custom_contact_content',
                       'required'  => false,            
                   ),
                   array(
                       'type' => 'textarea',
                       'label'     => $this->translations['admin_map'],
                       'name' => 'custom_contact_mapcode',
                       'required'  => false,            
                   ),
                   array(
                   		'type' => 'hidden',
                   		'name' => 'handleAdminSubmission'
                   )
           	  
               ),          
               'submit' => array(
                   'title' => $this->translations['admin_submit'],
                   'class' => 'btn btn-primary',
                   'icon' => 'process-icon-save',
               ),          
           );
   
           $helper = new HelperForm();         
                   
           $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
           $helper->token = Tools::getAdminTokenLite('AdminModules');        
           $helper->default_form_language = $default_lang;
           $helper->allow_employee_form_lang = $default_lang;                
           $helper->show_toolbar = true;
           $helper->toolbar_scroll = true;
           $helper->submit_action = 'submit_link';
           $helper->fields_value = array(
           		'handleAdminSubmission' => 1,
           		'custom_contact_title' => Configuration::get('custom_contact_title'),
           		'custom_contact_content' => Configuration::get('custom_contact_content'),
           		'custom_contact_mapcode' => Configuration::get('custom_contact_mapcode'),
           	);
   
           return $helper->generateForm($fields_form);    
   	}	
}