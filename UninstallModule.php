<?php

if (!defined('_PS_VERSION_'))
    exit;

class InstallModule
{
	private $uninstall_operations = array();

	public function performInstallation()
	{
		foreach ($this->uninstall_operations as $operation) 
		{	
			if (!$this->{$operation}())
				return false;			
		}

		return true;
	}	
}