<?php

class ContactController extends ContactControllerCore
{	
	public function initContent()
	{
		parent::initContent();
		
		$this->context->smarty->assign(
				array(
					'cc_title' => Configuration::get('custom_contact_title'),
					'cc_content' => Configuration::get('custom_contact_content'),
					'cc_map_code' => Configuration::get('custom_contact_mapcode')
				)
			);

		$this->setTemplate(_PS_MODULE_DIR_ . 'customcontact/views/templates/front/ControllerView.tpl');
	}

	public function postProcess()
	{
		return null;
	}
}