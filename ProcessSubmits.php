<?php 

if (!defined('_PS_VERSION_'))
    exit;

class ProcessSubmits
{
	public function process()
	{
		if (Tools::isSubmit('handleAdminSubmission'))
			$this->handleAdminSubmission();
	}

	protected function handleAdminSubmission()
	{
		$array_update = array();
		
		$array_update['custom_contact_title'] = Tools::getValue('custom_contact_title');
		$array_update['custom_contact_content'] = Tools::getValue('custom_contact_content');
		$array_update['custom_contact_mapcode'] = Tools::getValue('custom_contact_mapcode');

		foreach ($array_update as $key => $value) 
		{
			if ($value) Configuration::updateValue($key, $value);
		}
	}	
}