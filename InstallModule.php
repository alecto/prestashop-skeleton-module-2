<?php

if (!defined('_PS_VERSION_'))
    exit;

class InstallModule
{
	private $install_operations = array();

	public function performInstallation()
	{
		foreach ($this->install_operations as $operation) 
		{	
			if (!$this->{$operation}())
				return false;			
		}

		return true;
	}	
}