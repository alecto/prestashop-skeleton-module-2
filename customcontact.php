<?php

if (!defined('_PS_VERSION_'))
    exit;

class CustomContact extends Module
{
	public function __construct()
    {
        $this->name = 'customcontact';
        $this->tab = 'others'; 
        $this->version = '1.0.0'; 
        $this->author = 'Marcom Interactive';
        $this->need_instance = 0; 
        $this->ps_versions_compliancy = array( 'min' => '1.6.1', 'max' => _PS_VERSION_ ); 
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Custom Contact'); 
        $this->description = $this->l('Displays custom contact page.');         
        $this->confirmUninstall = $this->l('Are you sure?');       
    }

    public function install()
    {
        if (!parent::install())
        {
            return false;
        }

        return true;

    }

    public function uninstall() 
    {
        if (!parent::uninstall()) 
        {
            return false;
        }
 
        return true;    
    }

    public function getContent()
    {
        $return = '';

        $this->requireClasses(array('ModuleForms', 'ProcessSubmits'));
        
        $submits = new ProcessSubmits();
        $submits->process();

        $forms = new ModuleForms($this);

        $return .= $forms->getAdminForm();

        $tiny = '                
            <script type="text/javascript">
                tinymce.init({
                    selector: "#custom_contact_content",
                    plugins: "table, image, link"
                });
            </script>
        
        ';

        return $return . $tiny;
    }

    protected function requireClasses(array $classes)
    {
        foreach ($classes as $require) 
        {        	
        	if (!class_exists($require))
            	require __DIR__ . '/' . $require . '.php';
        }
    }

    public function returnTranslations()
    {
    	return array(
    				'admin_top_title' => $this->l('Title'),
                    'admin_content' => $this->l('Content'),
                    'admin_map' => $this->l('Map code'),
                    'admin_submit' => $this->l('Submit'),
    			);
    }
}