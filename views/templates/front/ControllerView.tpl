<div class="custom-contact row">
	<h2>{$cc_title}</h2>
	<div class="col-xs-12 col-md-7 map-field" style="padding: 0">		
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2548.2724198840547!2d18.754370715794117!3d50.305508079455564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4711321f2f301c71%3A0x5bf755ef5e0c2a3e!2sNails+Company!5e0!3m2!1spl!2spl!4v1457347787278" width="950" height="558px" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<div class="col-xs-12 col-md-5 content-field" style="padding-left: 100px">
		{$cc_content|unescape:'html':'UTF-8'}
	</div>
</div>